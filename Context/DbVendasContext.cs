using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.src.Models;

namespace tech_test_payment_api.Context;

public class DbVendasContext : DbContext
{
    public DbVendasContext(DbContextOptions<DbVendasContext> options) : base(options)
    {

    }
    public DbSet<Vendedor> TabelaVendedores { get; set; }
    public DbSet<Pedido> TabelaPedidos { get; set; }
}