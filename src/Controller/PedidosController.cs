using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using tech_test_payment_api.src.Models;

namespace tech_test_payment_api.src.Controller;

[ApiController]
[Route("api/[controller]")]
public class PedidosController : ControllerBase
{
    private readonly DbVendasContext _context;

    public PedidosController(DbVendasContext context)
    {
        _context = context;
    }

    // GET: MÉTODO PARA LISTAR TODOS OS PEDIDOS DA TABELA PEDIDOS
    [HttpGet("ListarTodos")]
    public async Task<ActionResult<IEnumerable<Pedido>>> GetTabelaPedidos()
    {
        return await _context.TabelaPedidos.ToListAsync();
    }

    // GET: MÉTODO PARA BUSCAR PEDIDO POR ID
    [HttpGet("BuscarPor{id}")]
    public async Task<ActionResult<Pedido>> GetPedido(int id)
    {
        var pedido = await _context.TabelaPedidos.FindAsync(id);

        if (pedido == null)
        {
            return NotFound();
        }

        return pedido;
    }

    // PUT: MÉTODO PARA ALTERAR O STATUS 

    [HttpPut("AlterarStatus{id}")]
    public async Task<IActionResult> PutPedido(int id, string status, Pedido pedido)
    {

        if (id == 0)
            return BadRequest("Campo id é necessário para buscar o pedido!");
        if (status == null)
            return BadRequest("Informe o novo status do pedido!");
        string[] statusBd1 = new string[2];

        // BUSCAR O PEDIDO PARA FAZER A ALTERAÇAO
        Pedido pedidoBanco = await _context.TabelaPedidos.FindAsync(id);

        // SE O PEDIDO ESTIVER EM ENVIADO PARA A TRANSPORTADORA OU CANCELADO NÃO SERÁ POSSÍVEL FAZER ALTERAÇÃO
        if (pedidoBanco.StatusDisponíveis.ToUpper() == "ENVIADO PARA TRANSPORTADORA" || pedidoBanco.StatusDisponíveis.ToUpper() == "CANCELADO")
        {
            return BadRequest($"O pedido em tela não aceita modificações no seu status: {pedidoBanco.StatusDoPedido}");
        }

        statusBd1 = pedidoBanco.StatusDisponíveis.Split(",");
        statusBd1[0].Trim();
        statusBd1[1].Trim();

        /* 
            VERIFICA AS POSSIBILIDADES DE ALTERAÇÃO:

                Aguardando pagamento - Pagamento Aprovado
                Aguardando pagamento - Cancelado
                Pagamento Aprovado - Enviado para Transportadora
                Pagamento Aprovado - Cancelado
                Enviado para Transportador - Entregue
        */

        if (status.ToUpper() != statusBd1[0].ToUpper() && status.ToUpper() != statusBd1[1].ToUpper())
        {
            return BadRequest($"O pedido em tela só aceita um dos status: {statusBd1[0]} ou {statusBd1[1]}");
        }

        DateTime data = DateTime.Now;

        data.ToShortDateString();
        pedidoBanco.StatusDoPedido = status;
        pedidoBanco.DataDaUltimaAtualizacao = data;

        if (status.ToUpper() == "PAGAMENTO APROVADO")
        {
            pedidoBanco.StatusDisponíveis = "Enviado para Transportadora, Cancelado";
        }
        else if (status.ToUpper() == "ENVIADO PARA TRANSPORTADORA")
        {
            pedidoBanco.StatusDisponíveis = "Entregue";
        }

        _context.Entry(pedidoBanco).State = EntityState.Modified;

        try
        {
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetPedido", new { id = pedido.Id }, pedido);

        }
        catch (DbUpdateConcurrencyException ex)
        {   
            if (!PedidoExists(id))
            {
                return NotFound();
            }
            else
            {
                return BadRequest(ex.ToString());
            }
        }
    }

    // POST: MÉTODO PARA CRIAR PEDIDO
    [HttpPost("NovoPedido")]
    public async Task<ActionResult<Pedido>> PostPedido(Pedido pedido, int vendedorId, string itens)
    {
        if (vendedorId == 0)
        {
            return BadRequest("Campo Id do vendedor é necessário!");
        }

        DateTime data = DateTime.Now;

        data.ToShortDateString();
        pedido.VendedorId = vendedorId;
        pedido.StatusDoPedido = "Aguardando Pagamento";
        pedido.StatusDisponíveis = "Pagamento Aprovado, Cancelado";
        pedido.DataDoPedido = data;
        pedido.DataDaUltimaAtualizacao = data;
        pedido.Istens = itens;

        try
        {
            _context.TabelaPedidos.Add(pedido);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetPedido", new { id = pedido.Id }, pedido);
        }
        catch (DbUpdateConcurrencyException ex)
        {
            return BadRequest(ex.ToString());
        }

    }

    // VERIFICA SE O PEDIDO EXISTE
    private bool PedidoExists(int id)
    {
        return _context.TabelaPedidos.Any(e => e.Id == id);
    }
}