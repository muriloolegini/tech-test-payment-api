namespace tech_test_payment_api.src.Models;

public class Vendedor
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int CPF { get; set; }
    public string Email { get; set; }
    public string Telefone { get; set; }
}