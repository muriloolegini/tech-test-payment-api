namespace tech_test_payment_api.src.Models;

public class Pedido
{
    public int Id { get; set; }
    public DateTime DataDoPedido { get; set; }
    public int VendedorId { get; set; }
    public string Istens { get; set; }
    public string StatusDisponíveis { get; set; }
    public string StatusDoPedido { get; set; }
    public DateTime DataDaUltimaAtualizacao { get; set; }
}